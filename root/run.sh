#!/bin/bash
#$1 - Registry Username 
#$2 - Registry Password
#$3 - Registry to login to
#$4 - Command to run

echo $1
echo $2
echo $3
echo $4

echo "Logging into ${3}..."
REGISTRY_USERNAME=${1}
REGISTRY_PASSWORD=${2}
echo "${REGISTRY_PASSWORD}" | docker login --username "${REGISTRY_USERNAME}" --password-stdin "${3}"
echo "Running 'docker ${4}' command..."
docker $4
